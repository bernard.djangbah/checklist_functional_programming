package sort

import scala.annotation.tailrec

object Sort {
  def main(args: Array[String]): Unit = {
    val arr = List(8,5,87,3,5,7,2,5,8,10)
    println(mergeSort(arr))
  }

  def mergeSort(array: List[Int]): List[Int] = {
    if (array.length < 2) array
    else {
      val h = array.length / 2
      merge(mergeSort(array.slice(0, h)), mergeSort(array.slice(h, array.length)))
    }
  }

  def merge(arr1: List[Int], arr2: List[Int]): List[Int] = {

    def tailMerge(a1: List[Int], a2: List[Int], acc: List[Int]): List[Int] = {
      a1
    }
    if (arr1.isEmpty) arr2
    else if (arr2.isEmpty) arr1
    else if (arr1.head < arr2.head) merge(arr1.tail, arr2).::(arr1.head)
    else merge(arr1, arr2.tail).::(arr2.head)
  }
}
