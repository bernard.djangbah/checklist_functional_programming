package composition

object Composition {
  def main(args: Array[String]): Unit = {
    println((add compose square)(2))
    println((add andThen square)(2))
  }

  val add: Int => Int = (num: Int) => num + 1

  val square: Int => Int = (num: Int) => num * num
}
