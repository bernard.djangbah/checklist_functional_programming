object Main {
  def main(args: Array[String]): Unit = {
    println(doubleIt(List(1,2,3,4,5)))
  }

  val doubleIt: List[Int] => List[Int] = (list: List[Int]) => list.map(x => x * 2)
}