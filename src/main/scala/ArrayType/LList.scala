package ArrayType

trait MyLList[+T] {
  def head: T
  def tail: MyLList[T]
  def isEmpty: Boolean
  def length: Int
  def contains[S >: T](e: S): Boolean
  def ::[S >: T](e: S): MyLList[S]
  def ++[S >: T](list: MyLList[S]): MyLList[S]
  def map[S >: T, R](f: S => R): MyLList[R]
  def flatmap[S >: T, R](f: S => MyLList[R]): MyLList[R]
  def appendAll[S >: T](list: MyLList[S]): MyLList[S]
  def foreach[S >: T](f: S => Unit): Unit
  def printElements: String
}

case object Empty extends MyLList[Nothing] {
  lazy val head: Nothing = throw new NoSuchElementException("head doesn't exist on empty list!")

  lazy val tail: MyLList[Nothing] = throw new UnsupportedOperationException("tail doesn't exist on empty list")

  override val isEmpty: Boolean = true

  override val length: Int = 0

  override def contains[S >: Nothing](e: S): Boolean = false

  override def ::[S >: Nothing](e: S): MyLList[S] = Cons(e, this)

  override def ++[S >: Nothing](list: MyLList[S]): MyLList[S] = list

  override def map[S >: Nothing, R](f: S => R): MyLList[R] = this

  override def flatmap[S >: Nothing, R](f: S => MyLList[R]): MyLList[R] = this

  override def appendAll[S >: Nothing](list: MyLList[S]): MyLList[S] = this

  override def foreach[S >: Nothing](f: S => Unit): Unit = ()

  override def printElements: String = ""

  override def toString: String = "MyLList()"
}

case class Cons[+T](head: T, tail: MyLList[T]) extends MyLList[T] {
  override val isEmpty: Boolean = false

  override val length: Int = 1 + tail.length

  override def contains[S >: T](e: S): Boolean = if (this.head == e) true else tail.contains(e)

  override def ::[S >: T](e: S): MyLList[S] = Cons(e, this)

  override def ++[S >: T](list: MyLList[S]): MyLList[S] = head :: tail ++ list

  override def map[S >: T, R](f: S => R): MyLList[R] = f(head) :: tail.map(f)

  override def flatmap[S >: T, R](f: S => MyLList[R]): MyLList[R] = f(head) ++ tail.flatmap(f)

  override def appendAll[S >: T](list: MyLList[S]): MyLList[S] = this ++ list

  override def foreach[S >: T](f: S => Unit): Unit = {
    f(head)
    tail.foreach(f)
  }

  override def printElements: String = "" + head + (if (tail.isEmpty) "" else ", ") + tail.printElements

  override def toString: String = "MyLList(" + printElements + ")"
}

case object MyLList {
  def apply[T](a: T*): MyLList[T] = if (a.isEmpty) Empty else Cons(a.head, apply(a.tail:_*))
}