package accFunc

import scala.annotation.tailrec

object Func extends App {
  val arr = Array(1,2,3,4,5,6)

  val res = accumFunc(arr, 0, (x: Int) => x)
  println(res)

  println(accumFunc(Array(), 10, (x: Int) => x))

  println(accumFunc(arr, 10, (x: Int) => x))


  @tailrec
  def accumFunc[T, R](arr: Array[T], acc: Int, f: T => Int): Int = {
    if (arr.isEmpty) acc
    else {
      val sum: Int = f(arr.head) + acc
      accumFunc(arr.tail, sum, f)
    }
  }
}