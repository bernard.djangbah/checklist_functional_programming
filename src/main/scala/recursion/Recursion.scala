package recursion

import scala.annotation.tailrec

object Recursion extends App {
  val arr = Array(1,2,3,4,5,6)
  val str = Array("Bernard", "Tetteh", "Djangbah")

  println(countArr(arr))
  println(countArr(str))

  def countArr[T](arr: Array[T]): Int = {
    @tailrec
    def accCountArr[T](array: Array[T], acc: Int) :Int = {
      if (array.isEmpty) acc
      else accCountArr(array.tail, acc + 1)
    }
    accCountArr(arr, 0)
  }
}