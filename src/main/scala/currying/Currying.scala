package currying

object Currying {
  def main(args: Array[String]): Unit ={
    println(addThreeNums(1,5,6))
    println(addThreeNumsCurrying(1)(5)(6))
  }

  def addThreeNums(a: Int, b: Int, c: Int): Int =
    a + b + c

  def addThreeNumsCurrying(a: Int) = (b: Int) => (c: Int) => a + b + c
}
